package cn.com.sandi.wechatcontrol.service;

import cn.com.sandi.wechatcontrol.model.UserGroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【user_group_relation】的数据库操作Service
* @createDate 2023-04-04 17:26:29
*/
public interface UserGroupRelationService extends IService<UserGroupRelation> {

}
