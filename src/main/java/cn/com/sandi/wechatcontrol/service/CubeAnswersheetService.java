package cn.com.sandi.wechatcontrol.service;

import cn.com.sandi.wechatcontrol.model.CubeAnswersheet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【cube_answersheet】的数据库操作Service
* @createDate 2023-03-31 10:47:34
*/
public interface CubeAnswersheetService extends IService<CubeAnswersheet> {

}
