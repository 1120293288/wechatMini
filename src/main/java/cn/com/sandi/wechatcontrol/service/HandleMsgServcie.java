package cn.com.sandi.wechatcontrol.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.print.attribute.standard.JobSheets;

/**
 * @Description //消息处理
 * @Date 2023/3/23 11:15
 * @Author hjs
 **/
@Service
public interface HandleMsgServcie {

    JSONObject caseMesseage(JSONObject msg);


    JSONObject passFriendAddRequest(JSONObject msg,JSONObject msgBody);

//    JSONObject pullGroupSendReport(JSONObject msg, JSONArray msgList);

    JSONObject sendCircleByTextPic(String port, String text, String picPath);


    JSONObject fowardAppMsg(String port,String toWxid,String imgPath,String topTitle,String title);
}
