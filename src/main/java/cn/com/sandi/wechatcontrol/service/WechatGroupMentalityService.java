package cn.com.sandi.wechatcontrol.service;

import cn.com.sandi.wechatcontrol.model.WechatGroupMentality;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【wechat_group_mentality】的数据库操作Service
* @createDate 2023-03-27 11:08:16
*/
public interface WechatGroupMentalityService extends IService<WechatGroupMentality> {

}
