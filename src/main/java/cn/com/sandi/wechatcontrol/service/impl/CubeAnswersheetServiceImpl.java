package cn.com.sandi.wechatcontrol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.sandi.wechatcontrol.model.CubeAnswersheet;
import cn.com.sandi.wechatcontrol.service.CubeAnswersheetService;
import cn.com.sandi.wechatcontrol.mapper.CubeAnswersheetMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【cube_answersheet】的数据库操作Service实现
* @createDate 2023-03-31 10:47:34
*/
@Service
public class CubeAnswersheetServiceImpl extends ServiceImpl<CubeAnswersheetMapper, CubeAnswersheet>
    implements CubeAnswersheetService{

}




