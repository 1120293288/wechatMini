package cn.com.sandi.wechatcontrol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.sandi.wechatcontrol.model.CubeSigner;
import cn.com.sandi.wechatcontrol.service.CubeSignerService;
import cn.com.sandi.wechatcontrol.mapper.CubeSignerMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【cube_signer】的数据库操作Service实现
* @createDate 2023-03-28 14:58:50
*/
@Service
public class CubeSignerServiceImpl extends ServiceImpl<CubeSignerMapper, CubeSigner>
    implements CubeSignerService{

}




