package cn.com.sandi.wechatcontrol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.sandi.wechatcontrol.model.UserGroupRelation;
import cn.com.sandi.wechatcontrol.service.UserGroupRelationService;
import cn.com.sandi.wechatcontrol.mapper.UserGroupRelationMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【user_group_relation】的数据库操作Service实现
* @createDate 2023-04-04 17:26:29
*/
@Service
public class UserGroupRelationServiceImpl extends ServiceImpl<UserGroupRelationMapper, UserGroupRelation>
    implements UserGroupRelationService{

}




