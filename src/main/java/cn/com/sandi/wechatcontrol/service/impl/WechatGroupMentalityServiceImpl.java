package cn.com.sandi.wechatcontrol.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.sandi.wechatcontrol.model.WechatGroupMentality;
import cn.com.sandi.wechatcontrol.service.WechatGroupMentalityService;
import cn.com.sandi.wechatcontrol.mapper.WechatGroupMentalityMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【wechat_group_mentality】的数据库操作Service实现
* @createDate 2023-03-27 11:08:16
*/
@Service
public class WechatGroupMentalityServiceImpl extends ServiceImpl<WechatGroupMentalityMapper, WechatGroupMentality>
    implements WechatGroupMentalityService{

}




