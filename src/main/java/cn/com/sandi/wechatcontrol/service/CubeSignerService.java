package cn.com.sandi.wechatcontrol.service;

import cn.com.sandi.wechatcontrol.model.CubeSigner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【cube_signer】的数据库操作Service
* @createDate 2023-03-28 14:58:50
*/
public interface CubeSignerService extends IService<CubeSigner> {

}
