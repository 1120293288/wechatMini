package cn.com.sandi.wechatcontrol.service.impl;

import cn.com.sandi.wechatcontrol.listener.WechatMiniProducer;
import cn.com.sandi.wechatcontrol.mapper.CubeAnswersheetMapper;
import cn.com.sandi.wechatcontrol.mapper.CubeSignerMapper;
import cn.com.sandi.wechatcontrol.mapper.UserGroupRelationMapper;
import cn.com.sandi.wechatcontrol.mapper.WechatGroupMentalityMapper;
import cn.com.sandi.wechatcontrol.model.*;
import cn.com.sandi.wechatcontrol.service.HandleMsgServcie;
import cn.com.sandi.wechatcontrol.utils.CommonUtils;
import cn.com.sandi.wechatcontrol.utils.RequestUtils;
import cn.com.sandi.wechatcontrol.utils.ResposeData;
import cn.com.sandi.wechatcontrol.utils.constant.MsgConstant;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

/**
 * @Description //处理消息
 * @Date 2023/3/23 11:22
 * @Author hjs
 **/
@Slf4j
@Service
public class HandleMsgServcieImpl implements HandleMsgServcie {

    @Autowired
    public WechatGroupMentalityMapper wechatGroupMentalityMapper;

    @Autowired
    public CubeAnswersheetMapper cubeAnswersheetMapper;

    @Autowired
    private RedissonClient redissonClient;

    @Qualifier("activiyMqQueueOne")
    @Autowired
    private ActiveMQQueue activeMQQueueOne;

    @Qualifier("activiyMqQueueTwo")
    @Autowired
    private ActiveMQQueue activiyMqQueueTwo;

    @Autowired
    private WechatMiniProducer wcMiniProducer;

    @Autowired
    private CubeSignerMapper cubeSignerMapper;

    @Autowired
    private UserGroupRelationMapper userGroupRelationMapper;
   /**
    * @Author hjs
    * @Description 消息分类
    * @Param [msg] 微信消息
    * @return
    **/
    @Override
    public JSONObject caseMesseage(JSONObject msg) {
        JSONArray msgList = msg.getJSONArray("msglist");
        JSONObject msgBody = msgList.getJSONObject(0);
        if (msgBody != null){
            switch (msgBody.getInteger("msgtype")){
                case MsgConstant.TEXT_MSG :
//                    //添加好友成功消息{"sendorrecv":"2","selfwxid":"wxid_eqdui86ls93l22","msglist":[{"index":"1","time":"2023-03-31 09:57:14","msgtype":"1","msgsvrid":"6300272267702617444","msg":"我通过了你的朋友验证请求，现在我们可以开始聊天了","fromtype":"1","fromid":"wxid_ye13igmg42z622","toid":"wxid_eqdui86ls93l22"},{"index":"2","time":"1970-01-01 08:00:00","msgtype":"1","msgsvrid":"6300272267702617444","msg":"我通过了你的朋友验证请求，现在我们可以开始聊天了","fromtype":"1","fromid":"柠萌","toid":"NM"}],"ServerPort":"30001","msgnumber":"2"}
//                    if (msg.size() == 2 && "我通过了你的朋友验证请求，现在我们可以开始聊天了".equals(msgBody.getString("msg") )){
//                        //拉群发个人测评报告
//                        pullGroupSendReport(msg,msgList);
//                    }
//                    //会员选择个人测评报告  fromtype个人消息=1 群消息=2 , fromid谁发的就是谁的wxid
//                    if (("#个人报告").equals(msgBody.getString("msg")) && "2".equals(msgBody.getString("fromtype"))
//                            && StringUtils.isNotBlank(msgBody.getString("fromid"))){
//
//                        MessageModel messageModel = new MessageModel(msg.toJSONString(),msgBody.getString("fromid"),"queryReport");
//                        Random random = new Random();
//                        long delayTime = random.nextInt(5000) + 3;
//                        //查询用户答过的问卷，发到微信群让用户选择问卷
//                        wcMiniProducer.delaySend(activeMQQueue,messageModel,delayTime);
//                    }
//                    //如果是发送选择
//                    if ("#".equals(msgBody.getString("msg").substring(0,1)) && StringUtils.isNumeric(msgBody.getString("msg").substring(1))){
//                        MessageModel messageModel = new MessageModel(msg.toJSONString(),msgBody.getString("fromid"),"sendReport");
//                        Random random = new Random();
//                        long delayTime = random.nextInt(5000) + 3;
//                        //查询用户答过的问卷，发到微信群让用户选择问卷
//                        wcMiniProducer.delaySend(activeMQQueue,messageModel,delayTime);
//                    }
                    break;
                case MsgConstant.FRIEND_REQUEST_MSG : passFriendAddRequest(msg,msgBody);break;

                default: break;
            }
        }
        return msg;
    }

    /**
     * @Author hjs
     * @Description 收到好友通过消息后的处理
     * @Param [msg] {"sendorrecv":"2","selfwxid":"wxid_eqdui86ls93l****","msglist":[{"index":"1","time":"2023-03-24 10:08:10","msgtype":"37","msgsvrid":"8488910242701208067","msg":"\u003cmsg fromusername\u003d\"wxid_ye13igmg42z*****\" encryptusername\u003d\"v3_020b3826fd03010000000000131300b63bfeec000000501ea9a3dba12f95f6b60a0536a1adb6c397be296afe2a6f267b763fe0f6bbd3e9a2ea8a66a5b18c34dcb497e85a9438f34fdf8d99f4a1fe04a3382981f6f2ea64cbc402d9859c19f6591ef529@stranger\" fromnickname\u003d\"柠萌\" content\u003d\"我是柠萌\" fullpy\u003d\"ningmeng\" shortpy\u003d\"NM\" imagestatus\u003d\"3\" scene\u003d\"45\" country\u003d\"\" province\u003d\"\" city\u003d\"\" sign\u003d\"\" percard\u003d\"1\" sex\u003d\"0\" alias\u003d\"\" weibo\u003d\"\" albumflag\u003d\"0\" albumstyle\u003d\"0\" albumbgimgid\u003d\"\" snsflag\u003d\"257\" snsbgimgid\u003d\"\" snsbgobjectid\u003d\"0\" mhash\u003d\"a7b15960c5bd945f71f89d367bbd474c\" mfullhash\u003d\"a7b15960c5bd945f71f89d367bbd474c\" bigheadimgurl\u003d\"http://wx.qlogo.cn/mmhead/ver_1/ZnBRVVCpaCfH7mSXibYwicwu7gIQEx9LxVtDWcuMp8vBPibI1Gak4cNJ5jwMGjcYmYqAPLiaCkYnh5G0xnE4acsR4v1p6Y5NLt0MTH9JebEcMMo/0\" smallheadimgurl\u003d\"http://wx.qlogo.cn/mmhead/ver_1/ZnBRVVCpaCfH7mSXibYwicwu7gIQEx9LxVtDWcuMp8vBPibI1Gak4cNJ5jwMGjcYmYqAPLiaCkYnh5G0xnE4acsR4v1p6Y5NLt0MTH9JebEcMMo/96\" ticket\u003d\"v4_000b708f0b04000001000000000053bedb82aa98de4c858d0a061d641000000050ded0b020927e3c97896a09d47e6e9eb97baf7c2925103f3a9eedd95d554e58571281ad705fd8897505f7cc3c6aeaabde3c158f7c09c6cc797f3095f5e4e02cdd72d13a2cb1bfd1776a121ecab30eb33b9ac3d0fc4302287a1996dd86b2a28ddd4a524ab4cdff2f140c82b60e21a2e8af46ad65c2b03e51b6@stranger\" opcode\u003d\"2\" googlecontact\u003d\"\" qrticket\u003d\"\" chatroomusername\u003d\"\" sourceusername\u003d\"\" sourcenickname\u003d\"\" sharecardusername\u003d\"\" sharecardnickname\u003d\"\" cardversion\u003d\"\" extflag\u003d\"0\"\u003e\u003cbrandlist count\u003d\"0\" ver\u003d\"800889277\"\u003e\u003c/brandlist\u003e\u003c/msg\u003e","fromtype":"1","fromid":"fmessage","toid":"wxid_eqdui86ls93l22"}],"ServerPort":"30001","msgnumber":"1"}
     * @Param [msgBody]消息内容 {"index":"1","time":"2023-03-24 10:08:10","msgtype":"37","msgsvrid":"8488910242701208067","msg":"\u003cmsg fromusername\u003d\"wxid_ye13igmg42z*****\" encryptusername\u003d\"v3_020b3826fd03010000000000131300b63bfeec000000501ea9a3dba12f95f6b60a0536a1adb6c397be296afe2a6f267b763fe0f6bbd3e9a2ea8a66a5b18c34dcb497e85a9438f34fdf8d99f4a1fe04a3382981f6f2ea64cbc402d9859c19f6591ef529@stranger\" fromnickname\u003d\"柠萌\" content\u003d\"我是柠萌\" fullpy\u003d\"ningmeng\" shortpy\u003d\"NM\" imagestatus\u003d\"3\" scene\u003d\"45\" country\u003d\"\" province\u003d\"\" city\u003d\"\" sign\u003d\"\" percard\u003d\"1\" sex\u003d\"0\" alias\u003d\"\" weibo\u003d\"\" albumflag\u003d\"0\" albumstyle\u003d\"0\" albumbgimgid\u003d\"\" snsflag\u003d\"257\" snsbgimgid\u003d\"\" snsbgobjectid\u003d\"0\" mhash\u003d\"a7b15960c5bd945f71f89d367bbd474c\" mfullhash\u003d\"a7b15960c5bd945f71f89d367bbd474c\" bigheadimgurl\u003d\"http://wx.qlogo.cn/mmhead/ver_1/ZnBRVVCpaCfH7mSXibYwicwu7gIQEx9LxVtDWcuMp8vBPibI1Gak4cNJ5jwMGjcYmYqAPLiaCkYnh5G0xnE4acsR4v1p6Y5NLt0MTH9JebEcMMo/0\" smallheadimgurl\u003d\"http://wx.qlogo.cn/mmhead/ver_1/ZnBRVVCpaCfH7mSXibYwicwu7gIQEx9LxVtDWcuMp8vBPibI1Gak4cNJ5jwMGjcYmYqAPLiaCkYnh5G0xnE4acsR4v1p6Y5NLt0MTH9JebEcMMo/96\" ticket\u003d\"v4_000b708f0b04000001000000000053bedb82aa98de4c858d0a061d641000000050ded0b020927e3c97896a09d47e6e9eb97baf7c2925103f3a9eedd95d554e58571281ad705fd8897505f7cc3c6aeaabde3c158f7c09c6cc797f3095f5e4e02cdd72d13a2cb1bfd1776a121ecab30eb33b9ac3d0fc4302287a1996dd86b2a28ddd4a524ab4cdff2f140c82b60e21a2e8af46ad65c2b03e51b6@stranger\" opcode\u003d\"2\" googlecontact\u003d\"\" qrticket\u003d\"\" chatroomusername\u003d\"\" sourceusername\u003d\"\" sourcenickname\u003d\"\" sharecardusername\u003d\"\" sharecardnickname\u003d\"\" cardversion\u003d\"\" extflag\u003d\"0\"\u003e\u003cbrandlist count\u003d\"0\" ver\u003d\"800889277\"\u003e\u003c/brandlist\u003e\u003c/msg\u003e","fromtype":"1","fromid":"fmessage","toid":"wxid_eqdui86ls93l22"}
     * @return
     **/
    @Override
    public JSONObject passFriendAddRequest(JSONObject msg,JSONObject msgBody) {

        //获取xml格式的好友申请信息
        String xmlMsg = msgBody.getString("msg");
        //挂载微信端口
        String serverPort = msg.getString("ServerPort");
        String selfWxid = msg.getString("selfwxid");
        HashMap<String, String> atri = CommonUtils.getXmlAtributeValue(xmlMsg);
        String friendWxid = atri.get("fromusername");
        String v3 = atri.get("encryptusername");
        String v4 = atri.get("ticket");
        String nickName = atri.get("fromnickname");
        log.info("passFriendAddRequest,通过好友申请，self微信:"+msg.getString("selfwxid")+"挂载微信端口："+serverPort+",好友参数:{friendWxid:"+friendWxid+",nickName:"+nickName+",v3:"+v3+",v4:"+v4+"}");
        Random random = new Random();
        //调用 通过好友请求
        boolean passFlag = RequestUtils.passFriendRquest(v3, v4, "8", serverPort);
        if (passFlag){
            HashMap<String, Object> nameMap = cubeSignerMapper.getCreateNameByNickName(nickName,selfWxid);

            RLock lock = null;
            if (nameMap != null) {
                String createName = String.valueOf(nameMap.get("createName"));
                String sheetId = String.valueOf(nameMap.get("sheetId"));
                String phone = String.valueOf(nameMap.get("phone"));
                try {
                    lock = redissonClient.getLock("intoGroud_"+selfWxid);
                    lock.lock(60, TimeUnit.SECONDS);
                    log.info("线程" + Thread.currentThread().getName() + "握锁");

                    //查询当天创建群的数量
                    Integer groupNum = wechatGroupMentalityMapper.countGroupNumInToday(selfWxid,1);
                    //一个微信 一天创建群的数量不能超过6个
                    if (groupNum > 6) {
                        return ResposeData.failure("一天创建群的数量不能超过6个");
                    } else {
                        QueryWrapper<UserGroupRelation> qw = new QueryWrapper<>();
                        qw.eq("wxid", friendWxid);
                        UserGroupRelation relation = userGroupRelationMapper.selectOne(qw);
                        //曾经加过好友和进过群
                        if (relation != null) {
                            QueryWrapper<WechatGroupMentality> queryWrapper = new QueryWrapper<>();
                            queryWrapper.eq("gid", relation.getGid());
                            WechatGroupMentality wechatGroup = wechatGroupMentalityMapper.selectOne(queryWrapper);
                            if (wechatGroup.getMemberNum() < 198) {
                                //拉进群
                                RequestUtils.pullFriendIntoGroup(relation.getGid(), friendWxid, serverPort, wechatGroup.getMemberNum() > 40 ? true : false);
                                TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));
                                //修改好友备注
                                String mark = createName + "-" + phone;
                                RequestUtils.modifyFriendRemark(friendWxid,mark, serverPort);
                                //发送个人测评报告
                                MessageModel messageModel = new MessageModel("firstIntoGroup", friendWxid, sheetId, serverPort);
                                long delayTime = random.nextInt(5000) + 3;
                                //延迟3-8秒 异步发送测评二维码
                                String wxidOne = CommonUtils.getPropertiesValue("application", "sendReportQueueWxidOne");
                                wcMiniProducer.delaySend(wxidOne.equals(wxidOne) == true ? activeMQQueueOne: activiyMqQueueTwo, messageModel, delayTime);
                            } else {
                                //全部群都满了或者没有创建群，otherWxid凑数微信
                                String otherWxid = CommonUtils.getPropertiesValue("reqUrl", "otherWxid");
                                String wxidList = friendWxid + "," + otherWxid;

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                //创建群聊
                                String gid = RequestUtils.createChatRoom(wxidList, serverPort);

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                //修改好友备注
                                RequestUtils.modifyFriendRemark(friendWxid, createName, serverPort);
                                if (gid != null) {
                                    TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                    //修改群昵称
                                    String groupName = CommonUtils.getPropertiesValue("reqUrl", "groupName");
                                    int groupCountByName = wechatGroupMentalityMapper.getGroupCountByName(groupName);
                                    String finalGroupName = groupName+(groupCountByName+1);
                                    RequestUtils.ChatRoomEditName(gid,finalGroupName,serverPort);

                                    TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                    //保存群到通讯录中
                                    RequestUtils.saveGroupToBook(gid, serverPort);
                                    //创建成功，记录群信息
                                    WechatGroupMentality wg = new WechatGroupMentality();
                                    wg.setId(UUID.randomUUID().toString());
                                    wg.setGid(gid);
                                    wg.setGroupType(1);
                                    wg.setMemberNum(1);
                                    wg.setGroupName(finalGroupName);
                                    wg.setCreateDate(new Date());
                                    wechatGroupMentalityMapper.insert(wg);
                                    //拉进群
                                    RequestUtils.pullFriendIntoGroup(gid, friendWxid, serverPort, false);
                                    //更新微信号和群的联系
                                    UserGroupRelation userGroupRelation = new UserGroupRelation();
                                    userGroupRelation.setUserGroupId(UUID.randomUUID().toString());
                                    userGroupRelation.setGid(gid);
                                    userGroupRelation.setWxid(friendWxid);
                                    userGroupRelation.setPhone(phone);
                                    Date date = new Date();
                                    userGroupRelation.setCreateData(date);
                                    userGroupRelation.setModifyDate(date);
                                    userGroupRelationMapper.updateById(userGroupRelation);
                                    MessageModel messageModel = new MessageModel("firstIntoGroup", friendWxid, sheetId, serverPort);
                                    long delayTime = random.nextInt(5000) + 3;
                                    //延迟3-8秒 异步发送测评二维码
                                    String wxidOne = CommonUtils.getPropertiesValue("application", "sendReportQueueWxidOne");
                                    wcMiniProducer.delaySend(wxidOne.equals(wxidOne) == true ? activeMQQueueOne: activiyMqQueueTwo, messageModel, delayTime);
                                } else {
                                    return ResposeData.failure("创建群聊失败");
                                }
                            }
                        } else {
                            //新加的好友
                            HashMap<String, String> groupMap = wechatGroupMentalityMapper.getCanAddGroup(selfWxid,1);
                            if (groupMap != null && groupMap.size() != 0) {
                                String gid = groupMap.get("gid");
                                int memberNum = Integer.parseInt(groupMap.get("member_num"));

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));

                                //拉进群
                                RequestUtils.pullFriendIntoGroup(gid, friendWxid, serverPort, memberNum > 40 ? true : false);
                                //增加进群人数
                                wechatGroupMentalityMapper.addGroupMemberNum(gid);
                                //保存微信号和群的联系
                                UserGroupRelation userGroupRelation = new UserGroupRelation();
                                userGroupRelation.setUserGroupId(UUID.randomUUID().toString());
                                userGroupRelation.setGid(gid);
                                userGroupRelation.setWxid(friendWxid);
                                userGroupRelation.setPhone(phone);
                                Date date = new Date();
                                userGroupRelation.setCreateData(date);
                                userGroupRelation.setModifyDate(date);
                                userGroupRelationMapper.insert(userGroupRelation);

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));
                                //修改好友备注
                                RequestUtils.modifyFriendRemark(friendWxid, createName + "-" + phone, serverPort);
                                //发送个人测评报告
                                MessageModel messageModel = new MessageModel("firstIntoGroup", friendWxid, sheetId, serverPort);
                                long delayTime = random.nextInt(5000) + 3;
                                //延迟3-8秒 异步发送测评二维码
                                String wxidOne = CommonUtils.getPropertiesValue("application", "sendReportQueueWxidOne");
                                wcMiniProducer.delaySend(wxidOne.equals(wxidOne) == true ? activeMQQueueOne: activiyMqQueueTwo, messageModel, delayTime);
                            } else {
                                //全部群都满了或者没有创建群，otherWxid凑数微信
                                String otherWxid = CommonUtils.getPropertiesValue("reqUrl", "otherWxid");
                                String wxidList = friendWxid + "," + otherWxid;

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                //创建群聊
                                String gid = RequestUtils.createChatRoom(wxidList, serverPort);

                                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                //修改好友备注
                                RequestUtils.modifyFriendRemark(friendWxid, createName+"-"+phone, serverPort);
                                if (gid != null) {
                                    TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                    //修改群昵称
                                    String groupName = CommonUtils.getPropertiesValue("reqUrl", "groupName");
                                    int groupCountByName = wechatGroupMentalityMapper.getGroupCountByName(groupName);
                                    String finalGroupName = groupName+(groupCountByName+1);
                                    RequestUtils.ChatRoomEditName(gid,finalGroupName,serverPort);

                                    TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                                    //保存群到通讯录中
                                    RequestUtils.saveGroupToBook(gid, serverPort);
                                    //创建成功，记录群信息
                                    WechatGroupMentality wg = new WechatGroupMentality();
                                    wg.setId(UUID.randomUUID().toString());
                                    wg.setGid(gid);
                                    wg.setGroupType(1);
                                    wg.setMemberNum(1);
                                    wg.setGroupName(finalGroupName);
                                    wg.setCreateDate(new Date());
                                    wechatGroupMentalityMapper.insert(wg);

                                    //保存微信号和群的联系
                                    UserGroupRelation userGroupRelation = new UserGroupRelation();
                                    userGroupRelation.setUserGroupId(UUID.randomUUID().toString());
                                    userGroupRelation.setGid(gid);
                                    userGroupRelation.setWxid(friendWxid);
                                    userGroupRelation.setPhone(phone);
                                    Date date = new Date();
                                    //保存人与群的关系
                                    userGroupRelation.setCreateData(date);
                                    userGroupRelation.setModifyDate(date);
                                    userGroupRelationMapper.insert(userGroupRelation);
                                    MessageModel messageModel = new MessageModel("firstIntoGroup", friendWxid, sheetId, serverPort);
                                    long delayTime = random.nextInt(5000) + 3;
                                    //延迟3-8秒 异步发送测评二维码
                                    String wxidOne = CommonUtils.getPropertiesValue("application", "sendReportQueueWxidOne");
                                    wcMiniProducer.delaySend(wxidOne.equals(wxidOne) == true ? activeMQQueueOne: activiyMqQueueTwo, messageModel, delayTime);
                                } else {
                                    return ResposeData.failure("创建群聊失败");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("", e);
                } finally {
                    lock.unlock();
                }
            }else {
                log.info(friendWxid+":"+nickName+",该好友的昵称，在前12小时内没有匹配得上的个人测评报告，不拉群里，不发送报告");
            }
        }
        return ResposeData.ok();
    }
    /**
     * 发送朋友圈
     * @param port
     * @param text
     * @param picPath
     * @return
     */
    @Override
    public JSONObject sendCircleByTextPic(String port, String text, String picPath) {
        StringBuilder media  = new StringBuilder();
            String pyq_xml_pic = null;

            String[] substring = picPath.replace("[]","").substring(1, picPath.length() - 1).split(",");
            if (substring.length > 9){
                return ResposeData.failure("不能超过9张图片");
            }
            for (String sub : substring) {

                pyq_xml_pic  = CommonUtils.getPropertiesValue("reqUrl", "pyq_xml_pic");
                if (StringUtils.isNotBlank(sub)){
                    JSONObject picJson = null;
                    String replace = sub.replace("\"", "");
                    picJson = RequestUtils.uploadCirclePic(port, replace);
                    if (StringUtils.isBlank(picJson.getString("pic_url_small"))){
                        log.info("picPath参数错误");
                        return ResposeData.failure("图片上传失败");
                    }
                    for (Map.Entry<String, Object> entry : picJson.entrySet()) {
                        String pic = StringUtils.isBlank(entry.getValue().toString()) == true ? "0" : entry.getValue().toString();
                        pyq_xml_pic = pyq_xml_pic.replaceAll("#" + entry.getKey() + "#", Matcher.quoteReplacement(pic));
                    }
                    media.append(pyq_xml_pic);
                }
            }

        String pyq_xml = CommonUtils.getPropertiesValue("reqUrl", "pyq_xml");

        if (StringUtils.isNotBlank(text)){
            pyq_xml = pyq_xml.replace("#text#",text);
        }else {
            pyq_xml = pyq_xml.replace("#text#","0");
        }
        if (media.length() > 0){
            pyq_xml =  pyq_xml.replace("#pyq_xml#",Matcher.quoteReplacement(media.toString()));
        }else {
            pyq_xml = pyq_xml.replace("#pyq_xml#","");
        }

        boolean flag = RequestUtils.sendCircleOfFriend(port, pyq_xml);
        if (flag){
            return ResposeData.ok();
        }else {
            return ResposeData.failure("发送失败");
        }
    }
    /**
     * 发送小程序
     * @param port
     * @param toWxid
     * @param topTitle
     * @param imgPath
     * @param title
     * @return
     */
    @Override
    public JSONObject fowardAppMsg(String port, String toWxid, String imgPath,String topTitle, String title) {

        String mini_xml = CommonUtils.getPropertiesValue("reqUrl", "mini_xml");
        String TopLabelStart = "<title>";
        String TopLabelEnd = "</title>";
        String titleLabelStart = "<sourcedisplayname>";
        String titleLabelEnd = "</sourcedisplayname>";
        String sub1 = mini_xml.substring(mini_xml.indexOf(TopLabelStart) + TopLabelStart.length(), mini_xml.indexOf(TopLabelEnd));
        String sub2 = mini_xml.substring(mini_xml.indexOf(titleLabelStart) + titleLabelStart.length(), mini_xml.indexOf(titleLabelEnd));

        mini_xml = mini_xml.replaceAll(sub1,topTitle == null ? "":topTitle);
        mini_xml = mini_xml.replaceAll(sub2,title == null ? "":title);
        JSONObject jsonObject = RequestUtils.sendForwardAppMsg(port, toWxid, imgPath == null ? "" : imgPath, mini_xml);
        if ("1".equals(jsonObject.getString("FowardAppMsg"))){
            return ResposeData.ok();
        }else {
            return ResposeData.failure("");
        }
    }
}
