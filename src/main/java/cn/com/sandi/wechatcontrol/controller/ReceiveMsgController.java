package cn.com.sandi.wechatcontrol.controller;

import cn.com.sandi.wechatcontrol.service.HandleMsgServcie;
import cn.com.sandi.wechatcontrol.utils.CommonUtils;
import cn.com.sandi.wechatcontrol.utils.RequestUtils;
import cn.com.sandi.wechatcontrol.utils.ResposeData;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@Slf4j
public class ReceiveMsgController {
    @Autowired
    public HandleMsgServcie handleMsgServcie;

    /**
     * @Author hjs
     * @Description 
     * @Param [msg] {"sendorrecv":"2","selfwxid":"wxid_eqdui123456769","msglist":[{"index":"1","time":"2023-03-23 11:56:15","msgtype":"1","msgsvrid":"2025589879153550118","msg":"阿松大塞1","fromtype":"1","fromid":"wxid_1jynriic460q123456","toid":"wxid_eqdui123456769"}],"ServerPort":"30001","msgnumber":"1"}
     * @return 
     **/
    @PostMapping(value = "/api/user/recive_msg")
    public String resceiveMsg(@RequestBody JSONObject msg){
        JSONObject jsonObject = new JSONObject();
        log.info(msg.toJSONString());
        if (msg != null){
            jsonObject = handleMsgServcie.caseMesseage(msg);
        }
        return jsonObject.toJSONString();
    }


    @PostMapping(value = "/api/user/recive_msg2")
    public String testResceiveMsg(@RequestBody JSONObject msg){
        log.info(msg.toJSONString());
        return "ok";
    }
}
