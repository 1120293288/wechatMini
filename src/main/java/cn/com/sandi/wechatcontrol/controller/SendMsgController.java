package cn.com.sandi.wechatcontrol.controller;

import cn.com.sandi.wechatcontrol.service.HandleMsgServcie;
import cn.com.sandi.wechatcontrol.utils.RequestUtils;
import cn.com.sandi.wechatcontrol.utils.ResposeData;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/Send")
public class SendMsgController {

    @Autowired
    public HandleMsgServcie handleMsgServcie;

    @PostMapping(value = "/SendCircleByPic")
    public String sendCircleByTextPic(@RequestParam(name = "port") String port, @RequestParam(name = "text") String text, @RequestParam(name = "picPath") String picPath){
        log.info("入参port："+port+",text:"+text+",picPath:"+picPath);
        if (StringUtils.isBlank(text) || StringUtils.isBlank(picPath)){
            return ResposeData.error("-1","text和picPath参数不能为空").toJSONString();
        }
        if (StringUtils.isBlank(port)){
            return ResposeData.error("-1","port参数不能为空").toJSONString();
        }

        JSONObject jsonObject = handleMsgServcie.sendCircleByTextPic(port, text, picPath);

        return jsonObject.toJSONString();
    }

    @PostMapping(value = "/SendTextMsg")
    public String sendTextMsg(@RequestParam(name = "port") String port,@RequestParam(name = "toWxid") String toWxid,@RequestParam(name = "text") String text){
        log.info("入参：port:"+port+",toWxid:"+toWxid+",text:"+text);
        if (StringUtils.isBlank(port) || StringUtils.isBlank(toWxid)){
            return ResposeData.error("-1","toWxid参数不能为空").toJSONString();
        }

        JSONObject jsonObject = RequestUtils.sendTextMsg(port, toWxid, text);
        return jsonObject.toJSONString();
    }

    @PostMapping(value = "/FowardAppMsg")
    public String fowardAppMsg(@RequestParam(name = "port") String port,@RequestParam(name = "toWxid") String toWxid,@RequestParam(name = "topTitle") String topTitle
                            ,@RequestParam(name = "title") String title,@RequestParam(name = "picPath") String picPath){
        log.info("入参：port:"+port+",toWxid:"+toWxid+",topTitle:"+topTitle+",title:"+title+",imgPath:"+picPath);
        if (StringUtils.isBlank(port) || StringUtils.isBlank(toWxid)){
            return ResposeData.error("-1","toWxid参数不能为空").toJSONString();
        }

        JSONObject jsonObject = handleMsgServcie.fowardAppMsg(port, toWxid, picPath, topTitle, title);
        return jsonObject.toJSONString();
    }

    @PostMapping(value = "/SendPicMsg")
    public String SendPicMsg(@RequestParam(name = "port") String port,@RequestParam(name = "toWxid") String toWxid,@RequestParam(name = "picPath")String picPath){
        if (StringUtils.isBlank(port) || StringUtils.isBlank(toWxid)){
            return ResposeData.failure("缺少必要参数").toJSONString();
        }

        boolean res = RequestUtils.sendPicMsg(toWxid,picPath,port);
        if (res){
            return ResposeData.ok().toJSONString();
        }else {
            return ResposeData.failure("发送失败").toJSONString();
        }
    }


}
