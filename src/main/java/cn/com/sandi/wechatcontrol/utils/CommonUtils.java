package cn.com.sandi.wechatcontrol.utils;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * @Description //TODO
 * @Date 2023/3/24 11:56
 * @Author hjs
 **/
public class CommonUtils {

    /**
     *  获取xml根标签的所有属性
     * @param xmlStr XML格式字符串
     * @return 标签属性map
     */
    public static HashMap<String, String> getXmlAtributeValue(String xmlStr){
        HashMap<String, String> atributeMap = new HashMap<>();
        StringReader sr = new StringReader(xmlStr);
        InputSource is = new InputSource(sr);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        org.w3c.dom.Document document = null;
        try {
            document = builder.parse(is);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element rootElement = document.getDocumentElement();
        //根节点所有属性
        NamedNodeMap attributes = rootElement.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Node item = attributes.item(i);
            atributeMap.put(item.getNodeName(),item.getNodeValue());
        }
        return atributeMap;
    }

    public static String getPropertiesValue(String fileName, String key){
        ResourceBundle bundle = ResourceBundle.getBundle(fileName);
        String vaule = bundle.getString(key);
        return vaule;
    }
}
