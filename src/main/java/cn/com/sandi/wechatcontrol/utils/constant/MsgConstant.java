package cn.com.sandi.wechatcontrol.utils.constant;

/**
 * @Description //TODO
 * @Date 2023/3/23 16:48
 * @Author hjs
 **/
public interface MsgConstant {
    /**
     * 文本消息
     */
    int TEXT_MSG = 1;
    /**
     * 图片消息
     */
    int IMG_MSG = 3;
    /**
     * 语音消息
     */
    int VOICE_MSG = 34;
    /**
     * 好友确认消息
     */
    int FRIEND_REQUEST_MSG = 37;
    /**
     * 好友推荐消息
     */
    int FRIEND_RECOMMEND_MSG = 40;
    /**
     * 共享名片
     */
    int SHARE_CARD = 42;
    /**
     * 视频消息
     */
    int MP4_MSG = 43;
    /**
     * 主动撤回
     */
    int ACTIVE_RECALL = 44;
    /**
     * 动画表情
     */
    int GIF_MSG = 47;
    /**
     * 位置消息
     */
    int LOCATION_MSG = 48;
    /**
     * APP分享链接/文件
     */
    int APP_SHARE_LINK_OR_FILE = 49;
    /**
     * VOIP消息
     */
    int VOIP_MSG = 50;
    /**
     * 微信初始化消息
     */
    int WECHAT_INIT_MSG = 51;
    /**
     * VOIP结束消息
     */
    int VOIP_END_MSG = 52;
    /**
     * VOIP邀请
     */
    int VOIP_INVITE = 53;
    /**
     * 小视频
     */
    int SMALL_MP4 = 62;
    /**
     * SYSNOTICE
     */
    int SYS_NOTICE = 9999;
    /**
     * 系统消息
     */
    int SYS_MSG = 10000;
    /**
     * 撤回消息
     */
    int RECALL_MSG = 10002;


}
