package cn.com.sandi.wechatcontrol.utils;


import lombok.AllArgsConstructor;


@AllArgsConstructor
public enum ResposeEnum {
    CODE("code"),
    MSG("msg"),
    VALUE("value"),
    SUCCESS("0","success"),
    FAILURE("-1","failure");

    public  String code;
    public  String desc;

    ResposeEnum(String desc){
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
