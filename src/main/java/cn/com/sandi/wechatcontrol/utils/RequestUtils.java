package cn.com.sandi.wechatcontrol.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * @Description 微信请求
 * @Date 2023/3/23 17:44
 * @Author hjs
 **/
@Slf4j
@Component
public class RequestUtils {
    private static String localUrl ;
    @Value("${weChatLocalUrl}")
    private String propertyLocalUrl;

    @PostConstruct
    private void init() {
        localUrl = propertyLocalUrl;
    }

    private static RestTemplate restTemplate;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        RequestUtils.restTemplate = restTemplate;
    }


    public static JSONObject sendRequest(String url,JSONObject params) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<JSONObject> httpEntity;
        if (params == null){
            httpEntity  = new HttpEntity<>(headers);
        }else {
            httpEntity = new HttpEntity<>(params, headers);
        }
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        JSONObject result =null;
        try {
             result = JSONObject.parseObject(responseEntity.getBody());
        }catch (Exception e){
            JSONArray array = JSON.parseArray(responseEntity.getBody());
            result = JSONObject.parseObject(array.get(0).toString());
        }
        log.info("请求接口{}，请求返回：{}", url, result);
        return result;
    }

    /**
     *通过好友请求
     * @param v3 好友申请的xml结构体的字段encryptusername,
     * @param v4 好友申请的xml结构体的字段ticket
     * @param role 朋友权限; 0:聊天、朋友圈、微信运动等 1:不让他（她）看 2:不看他（她） 3:不让他（她）看+不看他（她） 8:仅聊天
     * @param serverPort 挂载微信的端口
     * @return
     */
    public static boolean passFriendRquest(String v3,String v4,String role,String serverPort){

        String passFriend = CommonUtils.getPropertiesValue("reqUrl", "VerifyFriend");
        String url = localUrl + serverPort+ passFriend;
        JSONObject params = new JSONObject();
        params.put("v3",v3);
        params.put("v4",v4);
        params.put("role",role);

        try {
            //发送请求
            JSONObject result = sendRequest(url, params);
            if ("1".equals(result.getString("success"))){
                return true;
            }
        }catch (Exception e){
            log.error("发送好友通过请求失败",e);
            return false;
        }
        return false;
    }

    /**
     * 修改好友备注
     * @param wxid 微信id
     * @param mark 备注内容
     * @param serverPort 挂载微信的端口
     */
    public static boolean modifyFriendRemark(String wxid,String mark,String serverPort){
        String editFriendMark = CommonUtils.getPropertiesValue("reqUrl","EditFriendMark");

        String url = localUrl + serverPort + editFriendMark;
        JSONObject params = new JSONObject();
        params.put("wxid",wxid);
        params.put("mark",mark);
        try {
            JSONObject result = sendRequest(url, params);
            if ("1".equals(result.getString("EditFriendMark"))){
                return true;
            }
        }catch (Exception e){
            log.error("修改好友备注失败",e);
            return false;
        }
        return false;
    }

    /**
     * 添加好友进群，邀请好友进群
     * @param gid 群id
     * @param wxid 未超40人的接口wxid可以传多个 ,逗号分隔 wxid,wxid,wxid;
     * @param type true超40人 false 未超
     */
    public static boolean pullFriendIntoGroup(String gid ,String wxid,String serverPort,boolean type){

        if (type){
            //超40人
            String inviteGroupUrl = CommonUtils.getPropertiesValue("reqUrl", "InviteFriendToChatRoom");
            String url = localUrl + serverPort + inviteGroupUrl;
            JSONObject params = new JSONObject();
            params.put("gid",gid);
            params.put("wxid",wxid);
            try {
                JSONObject result = sendRequest(url, params);
                if ("1".equals(result.getString("success"))){
                    return true;
                }
            }catch (Exception e){
                log.error("邀请进去失败",e);
                return false;
            }
            return false;
        }else {
            //未超40人
            String intoGroupUrl = CommonUtils.getPropertiesValue("reqUrl", "AddFriendToChatRoom");
            String url = localUrl + serverPort + intoGroupUrl;
            JSONObject params = new JSONObject();
            params.put("gid",gid);
            params.put("wxidlist",wxid);
            try {
                JSONObject result = sendRequest(url, params);
                if ("1".equals(result.getString("success"))){
                    return true;
                }
            }catch (Exception e){
                log.error("添加好友进群失败",e);
                return false;
            }
            return false;
        }
    }

    /**
     *  创建群聊
     * @param wxidlist  wxid,wxid
     * @param serverPort
     * @return
     */
    public static String createChatRoom(String wxidlist,String serverPort){
        String createChatRoom = CommonUtils.getPropertiesValue("reqUrl", "CreateChatRoom");
        String url = localUrl + serverPort + createChatRoom;
        JSONObject params = new JSONObject();
        params.put("wxidlist",wxidlist);
        String gid = null;
        try {
            JSONObject result = sendRequest(url, params);
             gid = result.getString("gid");
            if (StringUtils.isBlank(gid)){
                log.error("url："+url+",参数wxidlist："+wxidlist+",创建群聊失败");
                return null;
            }
        }catch (Exception e){
            log.error("添加好友进群失败",e);
        }
        return gid;
    }

    /**
     * 发送图片
     * @param wxid
     * @param picpath 图片路径
     * @param serverPort
     * @return
     */
    public static boolean sendPicMsg(String wxid,String picpath,String serverPort){
        String sendPicMsg = CommonUtils.getPropertiesValue("reqUrl", "SendPicMsg");
        String url = localUrl + serverPort + sendPicMsg;
        JSONObject params = new JSONObject();
        params.put("wxid",wxid);
        params.put("picpath",picpath);
        params.put("diyfilename", UUID.randomUUID().toString().replace("-","")+".jpg");
        boolean flag = true;
        try {
            sendRequest(url, params);
        }catch (Exception e){
            flag = false;
            log.error("发送图片失败",e);
        }
        return flag;
    }

    /**
     *  保存群聊到通讯录中
     * @param gid 群id
     * @param serverPort
     * @return
     */
    public static boolean saveGroupToBook(String gid,String serverPort){
        String saveToContact = CommonUtils.getPropertiesValue("reqUrl", "SaveToContact");
        String url = localUrl + serverPort + saveToContact;
        JSONObject params = new JSONObject();
        params.put("gidorwxid",gid);
        boolean flag = true;
        try {
            sendRequest(url, params);
        }catch (Exception e){
            flag = false;
            log.error("保存群聊到通讯录失败",e);
        }
        return flag;
    }

    /**
     * 转发 、发送朋友圈
     *
     * @return
     */
    public static boolean sendCircleOfFriend(String port , String xmlStr){
        String sendTimeline = CommonUtils.getPropertiesValue("reqUrl", "SendTimeline");
        String url = localUrl + port + sendTimeline;
        JSONObject params = new JSONObject();
        params.put("send_pyq_xml",xmlStr);
        boolean flag = true;
        try {
            sendRequest(url, params);
        }catch (Exception e){
            flag = false;
            log.error("发送朋友圈失败",e);
        }
        return flag;
    }

    /**
     * 上传朋友圈图片
     * @return
     */
    public static JSONObject uploadCirclePic(String port,String picPath){
        String timelineUploadPic = CommonUtils.getPropertiesValue("reqUrl", "TimelineUploadPic");
        String url = localUrl + port + timelineUploadPic;

        JSONObject params = new JSONObject();
        params.put("PicPath",picPath);
        JSONObject jsonObject = null;
        try {
             jsonObject = sendRequest(url, params);
        }catch (Exception e){
            log.error("上传朋友圈图片失败",e);
        }
        return jsonObject;
    }

    /**
     * 获取个人信息
     * @param port
     * @return
     */
    public static JSONObject getOwnInfo(String port){
        String getSelfLoginInfo = CommonUtils.getPropertiesValue("reqUrl", "GetSelfLoginInfo");
        String url = localUrl + port + getSelfLoginInfo;
        JSONObject resultJson = null;
        try {
            resultJson = sendRequest(url, null);
        }catch (Exception e){
            log.error("获取个人信息失败",e);
        }
        return resultJson;
    }

    /**
     * 发送文本消息
     * @param port
     * @param toWxid
     * @param text
     * @return
     */
    public static JSONObject sendTextMsg(String port,String toWxid, String text){
        String sendTextMsg = CommonUtils.getPropertiesValue("reqUrl", "SendTextMsg");
        String url = localUrl + port + sendTextMsg;
        JSONObject params = new JSONObject();
        params.put("wxid",toWxid);
        params.put("msg",text);
        JSONObject resultJson = null;
        try {
            resultJson = sendRequest(url, params);
        }catch (Exception e){
            log.error("获取个人信息失败",e);
        }
        return resultJson;
    }

    /**
     * 发送小程序消息
     * @param port
     * @param towxid
     * @param imgpath
     * @param xml
     * @return
     */
    public static JSONObject sendForwardAppMsg(String port, String towxid,String imgpath, String xml){
        String fowardAppMsg = CommonUtils.getPropertiesValue("reqUrl", "FowardAppMsg");
        String url = localUrl + port + fowardAppMsg;
        JSONObject params = new JSONObject();
        params.put("towxid",towxid);
        params.put("imgpath",imgpath);
        params.put("xml",xml);
        JSONObject resultJson = null;
        try {
            resultJson = sendRequest(url, params);
        }catch (Exception e){
            log.error("获取个人信息失败",e);
        }
        return resultJson;
    }

    /**
     * 修改群聊名称
     * @param gid 群id
     * @param name
     * @param port
     * @return
     */
    public static JSONObject ChatRoomEditName(String gid, String name,String port){
        String chatRoomEditName = CommonUtils.getPropertiesValue("reqUrl", "ChatRoomEditName");
        String url = localUrl + port + chatRoomEditName;
        JSONObject params = new JSONObject();
        params.put("gid",gid);
        params.put("gname",name);
        JSONObject resultJson = null;
        try {
            resultJson = sendRequest(url, params);
        }catch (Exception e){
            log.error("修改群聊失败",e);
        }
        return resultJson;
    }
}
