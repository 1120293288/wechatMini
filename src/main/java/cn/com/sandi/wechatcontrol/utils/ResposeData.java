package cn.com.sandi.wechatcontrol.utils;

import com.alibaba.fastjson.JSONObject;

/**
 * @Description //TODO
 * @Date 2023/3/27 11:49
 * @Author hjs
 **/
public class ResposeData<T> {

    private String code;


    private String msg;


    private T value;

    public ResposeData(){ }

    public ResposeData(T value, String msg){
        this.code = "0";
        this.value = value;
        this.msg = msg;
    }

    public ResposeData(String code, String msg, T value){
        this.code = code;
        this.msg = msg;
        this.value = value;
    }

    /**
     * 用于前端获取数据或向前端响应数据的时候使用
     */
    public static <T> JSONObject success(T value){
        JSONObject resposeJson = new JSONObject();
        resposeJson.put(ResposeEnum.CODE.getDesc(), ResposeEnum.SUCCESS.getCode());
        resposeJson.put(ResposeEnum.MSG.getDesc(), ResposeEnum.SUCCESS.getDesc());
        if (value != null){
            resposeJson.put("value",value);
        }
        return resposeJson;
    }

    /**
     * 不需要返回数据，只返回成功的code
     */
    public static <T> JSONObject ok(){
        JSONObject ok = success(null);
        return ok;
    }

    /**
     * 不需要返回数据，只返回失败的code
     */
    public static <T> JSONObject failure(T value) {
        JSONObject resposeJson = new JSONObject();
        resposeJson.put(ResposeEnum.CODE.getDesc(), ResposeEnum.FAILURE.getCode());
        resposeJson.put(ResposeEnum.MSG.getDesc(), ResposeEnum.FAILURE.getDesc());
        if (value != null){
            resposeJson.put("value",value);
        }
        return resposeJson;
    }

    public static JSONObject error(String code, Object msg){
        JSONObject resposeJson = new JSONObject();
        resposeJson.put(ResposeEnum.CODE.getDesc(), code);
        resposeJson.put(ResposeEnum.MSG.getDesc(),msg);
        return resposeJson;
    }

}
