package cn.com.sandi.wechatcontrol.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ActiveMqConfig {

    @Value("${sendReportQueue}")
    public String sendReportQueueName;

    @Value("${sendReportQueueWxidOne}")
    public String wxidOne;

    @Value("${sendReportQueueWxidOne}")
    public String wxidTwo;

    @Bean
    public ActiveMQConnectionFactory factory(@Value("${spring.activemq.broker-url}") String url){
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(url);
        // 设置信任序列化包集合
        List<String> models = new ArrayList<>();
        models.add("cn.com.sandi.wechatsend.model");
        factory.setTrustedPackages(models);
        // 设置处理机制
        RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
        redeliveryPolicy.setMaximumRedeliveries(2); // 消息处理失败重新处理次数
        factory.setRedeliveryPolicy(redeliveryPolicy);
        return factory;
    }

    @Bean
    public ActiveMQQueue activiyMqQueueOne(){
        ActiveMQQueue activeMQQueue = new ActiveMQQueue(sendReportQueueName+"-"+wxidOne);
        return activeMQQueue;
    }

    @Bean
    public ActiveMQQueue activiyMqQueueTwo(){
        ActiveMQQueue activeMQQueue = new ActiveMQQueue(sendReportQueueName+"-"+wxidTwo);
        return activeMQQueue;
    }
}
