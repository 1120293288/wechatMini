package cn.com.sandi.wechatcontrol.mapper;

import cn.com.sandi.wechatcontrol.model.CubeSigner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
* @author Administrator
* @description 针对表【cube_signer】的数据库操作Mapper
* @createDate 2023-03-28 14:58:50
* @Entity cn.com.sandi.wechatmini.model.CubeSigner
*/
@Repository
public interface CubeSignerMapper extends BaseMapper<CubeSigner> {

    HashMap<String,Object> getCreateNameByNickName(String nickName,String selfWxid);

}




