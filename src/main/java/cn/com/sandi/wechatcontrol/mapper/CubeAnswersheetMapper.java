package cn.com.sandi.wechatcontrol.mapper;

import cn.com.sandi.wechatcontrol.model.CubeAnswersheet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author Administrator
* @description 针对表【cube_answersheet】的数据库操作Mapper
* @createDate 2023-03-31 10:47:34
* @Entity cn.com.sandi.wechatmini.model.CubeAnswersheet
*/
@Repository
public interface CubeAnswersheetMapper extends BaseMapper<CubeAnswersheet> {

}




