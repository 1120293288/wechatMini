package cn.com.sandi.wechatcontrol.mapper;

import cn.com.sandi.wechatcontrol.model.WechatGroupMentality;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

/**
* @author Administrator
* @description 针对表【wechat_group_mentality】的数据库操作Mapper
* @createDate 2023-03-27 11:08:16
* @Entity cn.com.sandi.wechatmini.model.WechatGroupMentality
*/
@Repository
public interface WechatGroupMentalityMapper extends BaseMapper<WechatGroupMentality> {

     /**
      * 查询当天创建群的数量
      * @param type 群类型
      * @return
      */
     Integer countGroupNumInToday(String wxid,Integer type);

     /**
      * 查询不超过200人的群
      * @return gid
      */
     HashMap<String ,String> getCanAddGroup(String wxid,Integer type);

     /**
      * 增加进群人数
      * @param gid
      * @return
      */
     int addGroupMemberNum(String gid);


     int getGroupCountByName(String name);
}




