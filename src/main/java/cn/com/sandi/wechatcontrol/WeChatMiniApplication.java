package cn.com.sandi.wechatcontrol;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@MapperScan("cn.com.sandi.wechatcontrol.mapper")
public class WeChatMiniApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeChatMiniApplication.class, args);
    }

}
