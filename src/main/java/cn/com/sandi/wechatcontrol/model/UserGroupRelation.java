package cn.com.sandi.wechatcontrol.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName user_group_relation
 */
@TableName(value ="user_group_relation")
@Data
public class UserGroupRelation implements Serializable {
    /**
     * 
     */
    private String userGroupId;

    /**
     * 
     */
    private String phone;

    /**
     * 微信原始id
     */
    private String wxid;

    /**
     * 群id
     */
    private String gid;

    /**
     * 
     */
    private Date createData;

    /**
     * 
     */
    private Date modifyDate;

    /**
     *
     */
    private String createName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}