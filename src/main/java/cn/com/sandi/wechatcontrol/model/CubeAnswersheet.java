package cn.com.sandi.wechatcontrol.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName cube_answersheet
 */
@TableName(value ="cube_answersheet")
@Data
public class CubeAnswersheet implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer answersheetId;

    /**
     * 
     */
    private Integer tenantsId;

    /**
     * 
     */
    private Integer organizerId;

    /**
     * 
     */
    private Integer questionnaireId;

    /**
     * 
     */
    private Integer answerPerson;

    /**
     * 
     */
    private Integer createId;

    /**
     * 
     */
    private String createName;

    /**
     * 
     */
    private Date createDate;

    /**
     * 
     */
    private Integer modifyId;

    /**
     * 
     */
    private String modifyName;

    /**
     * 
     */
    private Date modifyDate;

    /**
     * 测评时长(秒)
     */
    private Double timeSpend;

    /**
     * 0存疑1有效
     */
    private Integer isVaild;

    /**
     * 心里状态,1状态良好2状态欠佳或有焦虑倾向3心理焦虑较严重
     */
    private Integer mentalState;

    /**
     * 学习焦虑得分(标准分)
     */
    private Integer studyAnxiety;

    /**
     * 对人焦虑
     */
    private Integer meetAnxiety;

    /**
     * 孤独倾向
     */
    private Integer aloneTend;

    /**
     * 自责倾向
     */
    private Integer selfBlameTend;

    /**
     * 过敏倾向
     */
    private Integer allergyTend;

    /**
     * 躯体症状
     */
    private Integer somatization;

    /**
     * 恐怖倾向
     */
    private Integer dreadTend;

    /**
     * 冲动倾向
     */
    private Integer impulsionTend;

    /**
     * 谎言
     */
    private Integer lie;

    /**
     * 噩梦困扰,0无1有
     */
    private Integer nightmare;

    /**
     * 黑暗恐惧0无1有
     */
    private Integer darkFear;

    /**
     * 轻生念头0无1有
     */
    private Integer suicideThink;

    /**
     * 平均用时(秒)
     */
    private Double avgTimeSpend;

    /**
     * 标准分（总分）
     */
    private Integer standardScore;

    /**
     * 0未运算1已运算
     */
    private Integer state;

    private String wxid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}