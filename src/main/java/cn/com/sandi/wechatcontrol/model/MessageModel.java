package cn.com.sandi.wechatcontrol.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description 消息包装类
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageModel implements Serializable {

    private String title;

    private String wxid;

    private String answersheetId;

    private String serverPort;


}
