package cn.com.sandi.wechatcontrol.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName wechat_group_mentality
 */
@TableName(value ="wechat_group_mentality")
@Data
public class WechatGroupMentality implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;

    /**
     * 微信群id
     */
    private String gid;

    /**
     * 人数
     */
    private Integer memberNum;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 群用途:1心理健康问卷
     */
    private Integer groupType;

    /**
     * 群用昵称
     */
    private String groupName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}