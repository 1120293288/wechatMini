package cn.com.sandi.wechatcontrol.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName cube_signer
 */
@TableName(value ="cube_signer")
@Data
public class CubeSigner implements Serializable {
    /**
     * 
     */
    @TableId
    private String signerId;

    /**
     * 填写人学校
     */
    private String school;

    /**
     * 填写人班级
     */
    private String className;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 电话
     */
    private String answerPhone;

    /**
     * 填写问卷
     */
    private Integer questionnaireId;

    /**
     * 问卷id
     */
    private String createName;

    /**
     * 
     */
    private Date createDate;

    /**
     * 
     */
    private String nickname;

    /**
     * 
     */
    private String unionid;

    /**
     * 0未完成答题1完成答题
     */
    private Integer state;

    /**
     * 点击添加好友按钮的时间
     */
    private Date addTime;
    /**
     *
     */
    private Long organizerId;
    /**
     *
     */
    private Long tenantsId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}